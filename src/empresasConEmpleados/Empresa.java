package empresasConEmpleados;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Empresa {
	protected String nombre;

	protected List<Empleado> empleados = new ArrayList<>();

	public Empresa(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleados.add(empleado);
	}

	public List<Empleado> getEmpleados() {
		return empleados;
	}
	
	public Empleado empleadoConTel(String numero){
		 return empleados.stream().filter(empleado->empleado.getTelefono().equals(numero)).findAny().get();
	}
	
	public List<Empleado>empleadosEnViaje(){
		return empleados.stream().filter(empleado->empleado.estaEnViaje()).collect(Collectors.toList());
	}
	
	public boolean estaEnLaDireccion(String calle, String numero){
	return empleados.stream().anyMatch(empleado->empleado.estaEnDestino() && 
			empleado.getCalle().equals(calle.toUpperCase())&& 
			empleado.getNumero().equals(numero));
	}
}
