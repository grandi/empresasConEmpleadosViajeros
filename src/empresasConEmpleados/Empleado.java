package empresasConEmpleados;

public class Empleado {
	protected String nombre;
	protected String apellido;
	protected String telefono;
	protected String calle;
	protected String numero;
	protected Eventos eventos;

	public Empleado(String nombre, String apellido, String telefono) {
		this.apellido = apellido.toUpperCase();
		this.nombre = nombre.toUpperCase();
		this.telefono = telefono;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setEstado(Eventos evento) {
		this.eventos = evento;

	}

	public boolean estaEnViaje() {
		return this.eventos == Eventos.EN_VIAJE;
	}
	public boolean estaEnDestino(){
		return this.eventos==Eventos.EN_DESTINO;
	}

	public void setLlegaADireccion(String calle, String numero) {
		this.calle = calle.toUpperCase();
		this.numero = numero;
		this.setEstado(eventos.EN_DESTINO);//agregue  el estado
	}

	public String getCalle() {
		return calle;
	}

	public String getNumero() {
		return numero;
	}

	public Eventos getEventos() {
		return eventos;
	}

	public void seEncuentraEn() {
		if (this.estaEnDestino()){

			this.getCalle();
			this.getNumero();
		}

	}
	public void setSaleDeViaje(){// agregue el metodo
		this.setEstado(eventos.EN_VIAJE);
		this.calle= null;
		this.numero= null;
	}
}
